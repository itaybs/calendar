<?php

require __DIR__ . '/vendor/autoload.php';
session_start();
if (isset($_POST['submit'])){
$client = new Google_Client();
$client->setAuthConfigFile('client_secrets.json');
$client->addScope('https://www.googleapis.com/auth/calendar');

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  
  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

  $event = new Google_Service_Calendar_Event(array(
  'summary' => 'test event 2017',
  'location' => 'Jerusalem',
  'description' => 'Testing the google calendar API',
  /*'start' => array(
    'dateTime' => '2017-02-03T14:00:00+02:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'end' => array(
    'dateTime' => '2017-02-03T14:05:00+02:00',
    'timeZone' => 'America/Los_Angeles',
  ),*/
  
  'start' => array(
    'dateTime' => $_POST['start'].':00+02:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'end' => array(
    'dateTime' => $_POST['last'].':00+02:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  
  'recurrence' => array(
    'RRULE:FREQ=DAILY;COUNT=1'
  ),
  'attendees' => array(
    array('email' => 'avivfi91@gmail.com'),
    array('email' => 'example@example.com'),
  ),
  'reminders' => array(
    'useDefault' => FALSE,
    'overrides' => array(
      array('method' => 'email', 'minutes' => 24 * 60),
      array('method' => 'popup', 'minutes' => 10),
    ),
  ),
));
  
$calendarId = 'primary';
$event = $service->events->insert($calendarId, $event);
printf('Event created: %s\n', $event->htmlLink);
  
} else {
  $redirect_uri =  'http://itaybs.myweb.jce.ac.il/calender/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));

}}?>
<!DOCTYPE html>
<html lang="he">
<head>
	<title>Calendar</title>
	<meta charset="utf-8">
	<!--<link rel="stylesheet" type="text/css" href="style.css"> !-->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body class="body">
	<form role="form" method="POST" action="index.php">
		<header>
			<h2>Calendar</h2>
		</header>
		<div class="form-group">
			<label for="start">Start - date and time:</label>
			<input type="datetime-local" name="start" class="form-control" id="start"> 
		</div>
		<div class="form-group">
			<label for="last">Last- date and time:</label>
			<input type="datetime-local" name="last" class="form-control" id="last"  > 
		</div>
	<input type="submit" class="btn btn-default" value="submit" name="submit">
	</form>

</body>

</html>



